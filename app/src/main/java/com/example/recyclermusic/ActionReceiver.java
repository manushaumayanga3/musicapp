package com.example.recyclermusic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class ActionReceiver extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context,"recieved",Toast.LENGTH_SHORT).show();

        String action=intent.getStringExtra("action");
        if(action.equals("Play")){
            performActionPlay();
            Toast.makeText(context,"one",Toast.LENGTH_SHORT).show();
        }
        else if(action.equals("Pause")){
            performActionPause();
            Toast.makeText(context,"two",Toast.LENGTH_SHORT).show();

        }
        else if(action.equals("Previous")){
            performActionPrevious();
            Toast.makeText(context,"three",Toast.LENGTH_SHORT).show();

        }
        else if(action.equals("Next")){
            performActionNext();
            Toast.makeText(context,"four",Toast.LENGTH_SHORT).show();

        }

        //This is used to close the notification tray
        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(it);
    }

    public void performActionPlay(){
    Log.i("Notification1","Play");
        //Do logic for play

    }

    public void performActionPause(){
        Log.i("Notification1","Pause");
        //Do logic for pause

    }
    public void performActionPrevious(){
        Log.i("Notification1","Previous");
        //Do logic for previous

    }
    public void performActionNext(){
        Log.i("Notification1","Next");
        //Do logic for next

    }

    }
